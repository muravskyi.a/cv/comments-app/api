# Comments App API

__Comments App API__ is web service based on Symfony 5 framework as example of my using Symfony framework with implementing TDD & DDD methodologies and CQRS pattern. It's QUERY service of CQRS pattern implementation.

## Environments

- DATABASE_URL: by default mysql://root:root@127.0.0.1:3307/comments_test

- AWS_KEY: AWS IAM service user access key
- AWS_SECRET: AWS IAM user secret key
- AWS_REGION: Set in AWS region in which was created SQS queue and SNS topic
- AWS_TOPIC_ARN: Amazon Resource Name which is set in SQS service for created queue when click on it in SQS queues list
- AWS_SDK_VERSION: by default **2010-03-31**

## Run app with docker-compose

```
docker-compose up
```

## Testing

To test application execute command in your terminal
```bash
vendor/bin/phpunit
```

---

## Implementation steps

1. Install **Symfony 5**
   ```bash
   composer create-project symfony/skeleton api
   ```
2. Copy and past [Docker files](https://gitlab.com/muravskyi.a/cv/docker-files/php/dev-local-env/http) for running php, nginx and database in docker containers
3. Install PhpUnit bridge
   ```bash
   composer require --dev symfony/phpunit-bridge
   ```
4. Install doctrine bundle
   ```bash
   composer require doctrine/doctrine-bundle
   ```
5. Install doctrine orm
   ```bash
   composer require doctrine/orm
   ```
6. Install liip functional test bundle
   ```bash
   composer require liip/functional-test-bundle
   ```
7. Install liip test fixtures bundle
   ```bash
   composer require liip/test-fixtures-bundle
   ```
8. Configure VSCode IDE
   * Add extensions for [class generation](https://marketplace.visualstudio.com/items?itemName=damianbal.vs-phpclassgen)
   * Add extension for [xdebug](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug)
   * Add [settings](https://devilbox.readthedocs.io/en/latest/intermediate/configure-php-xdebug/linux/vscode.html#configure-vscode) in .vscode/launch.json to connect xdebug with docker container and configure xdebug in php.ini
9.  Add `IAmAlive` health check functionality
   * Add `tests/IAmAlive/IAmAliveControllerTest.php` test class with testing method
   * Add `src/IAmAlive/IAmAliveController.php` controller with requests handler function
   * Add route configuration in `config/routes.yaml`
10. Add `Comment` component functionality in DDD TDD way
   * Add tests DDD folders for `Comment` component: Domain, Infrastructure and Application.
   * Add test class for `CommentEndpoint` controllers `getCommentAsJson` method
      `tests/Comment/Infrastructure/CommentEndpointTest.php`
   * Add controller `CommentEndpoint` with `getCommentAsJson` method and plug for result
     `src/Comment/Infrastructure/CommentEndpoint.php`
   * Add test `CommentRepositoryTest` class which required in `CommentEndpoint` function
     `tests/Comment/Infrastructure/CommentRepositoryTest.php`
   * Add `CommentRepository` class with `getCommentById` method which required for `CommentEndpoint` functionality
     `src/Comment/Infrastructure/CommentRepository.php`
   * Add interface for `src/Comment/Domain/Repository/CommentRepositoryInterface.php` with method `getCommentById`
   * Set type of expected function result as this interface in `CommentRepositoryTest::getRepository()` method for correct IDE autocomplete
   * Update doctrine DB and orm configuration in `config/packages/doctrine.yaml` (charset, orm mapping)
   * Add orm xml configuration for Comment entity `src/Comment/Infrastructure/config/orm/Comment.orm.xml`
   * Add fixture `tests/Comment/Infrastructure/Fixtures/CommentFixture.php` for creating records in DB for testing.
   * Add fixture loading in `tests/Comment/Infrastructure/CommentRepositoryTest.php`
   * Add `config/services_test.yaml` configuration with `Test.CommentRepository` service which use alias on original `CommentRepository` configuration in `config/services.yaml`
   * Add configuration for `CommentRepository` in `config/services.yaml` with set using Doctrine ORM EntityRepository.
   * Add getters in Comment entity in `tests/Comment/Domain/Entity/Comment.php` for possibility to get properties which we will check in test.
     We don't use reflection instead getters here like in Adapter because we will use Comment props getters in API request handler which is a part of production code.
   * After running docker-compose for this service you will get db service without databases and tables. You can create `comment` and `comment_test` databases by using your db managment tool. I use [HeidiSQL](https://www.heidisql.com/download.php).
   * Set your databases hosts in `.env` and `.env.test` files.
   * To create Comment table in your default and test databases open terminal in [Adapter](https://gitlab.com/muravskyi.a/cv/comments-app/adapter) service directory and run migration by using this commands:

      ```bash
      bin/console doctrine:migrations:migrate
      bin/console doctrine:migrations:migrate --env=test
      ```

      **Adapter** service is responsible for data manipulations in database. That's why migrations for database are stored there.
      If you are using php from docker container you will need to run php container of Adapter service and execute this commands from there.
      For doing it execute commands in terminal with opened Adapter directory:

      Run adapter php container

      ```bash
      docker start adapter_php_1
      ```
      Then if you don't know your docker containers gateway get it from docker inspection in Gateway property of `networks` section.

      ```bash
      docker inspect adapter_php_1
      ```
      Set DB host of your docker containers gateway in .env and .env.test files.

      Open bash of your adapters php container end run migration from there

      ```bash
      docker exec -it adapter_php_1 bash
      ```

     * Run PHPUnit `CommentRepositoryTest` test.
     * Add constructor in `CommentEndpoint` and inject `CommentRepository` as input argument.
     * Add configuration for `CommentEndpoint` in  `config/services.yaml` for automatic injecting  `CommentRepository`.
     * Add fixture loading in `tests/Comment/Infrastructure/CommentEndpointTest.php`
     * Run test `CommentEndpoint` test
     * Let's add comment entity serialization in JSON and will move serialization responsibility from controller into entity.
     - Add method `aCommentCanBeSerialized` in `CommentTest` which will check json serialization of Comment entity.
     - Add implementation of `JsonSerializable` interface in Comment entity
     - Add method `jsonSerialize` in Comment entity
     - Run test CommentTest
     - Run other tests to check that CommentEndpoint test is not broken
   * Add `CommentPostEndpoint` functionality
      - Add `tests/Comment/Infrastructure/CommentPostEndpointTest.php` web test class
      - Add method `aCommentCanBePosted` in this class to check successful response of endpoint
      - Add rout for new endpoint in `config/routes.yaml`
      - Add src `src/Comment/Infrastructure/CommentPostEndpoint.php` which will handle comment post requests
      - Run test to check current state
      - Add test method `endpointReturnsFailedWhenNoPostDataGiven` in `CommentPostEndpointTest` to check correct data validation
      - Add value object `src/Comment/Domain/ValueObject/Comment.php` which will be used for event publishing
      - Add test for comment value object factory
      `tests/Comment/Domain/ValueObjectFactory/CommentFactoryTest.php`
      - Add src factory for comment value object
      `src/Comment/Domain/ValueObjectFactory/CommentFactory.php`
      - Register comment value object factory in `config/services_test.yaml` with using alias for getting it in tests
      - Register value object factory in `config/services.yaml`.
      - Add injecting comment value object factory `CommentFactory` into `CommentPostEndpoint` in `services.yaml`
      - Run value object `CommentFactoryTest`
      - Add test for comment upserted event value object factory
      `tests/Comment/Domain/ValueObjectFactory/CommentUpsertedEventFactoryTest.php`
      - Add src factory for comment upserted event value object
      `src/Comment/Domain/ValueObjectFactory/CommentUpsertedEventFactory.php`
      - Register comment upserted event value object factory in `config/services_test.yaml` with using alias
      - Register comment upserted event value object factory in `config/services.yaml`
      - Add injecting comment upserted event value factory `CommentUpsertedEventFactory` into `CommentPostEndpoint` in `services.yaml`
      - Run value object `CommentUpsertedEventFactoryTest`

  * Add service bus

      For sending events in service bus you can use AWS SQS/SNS services or other solutions like RabbitMQ, Kafka etc.

      I'm use in this example AWS. For connection API service with AWS I'm use library `bnnvara/simple-bus-aws-bridge-bundle`.

      - Install bundle for using AWS SQS

        For it installation execute command and execute receipt when it will be asking you about that.

        ```bash
        composer require bnnvara/simple-bus-aws-bridge-bundle
        ```

        I got exception when tried to install it in current version on stage `bin/console clean:cache`.
        As work around we need at first add configuration which described bellow, connected bundles and repeat installation.

      - Add `config/packages/simple_bus_aws_bridge.yaml` configuration file
      - Add bundles in `config/bundles.php`. If after installation

        ```
        ...
        SimpleBus\AsynchronousBundle\SimpleBusAsynchronousBundle::class => ['all' => true],
        JMS\SerializerBundle\JMSSerializerBundle::class => ['all' => true],
        SimpleBus\JMSSerializerBundleBridge\SimpleBusJMSSerializerBundleBridgeBundle::class => ['all' => true],
        BNNVARA\SimpleBusAwsBridgeBundle\SimpleBusAwsBridgeBundle::class => ['all' => true],
        SimpleBus\SymfonyBridge\SimpleBusCommandBusBundle::class => ['all' => true],
        SimpleBus\SymfonyBridge\SimpleBusEventBusBundle::class => ['all' => true],
        ...
        ```
      - Run composer installation if it was failed before
        ```bash
        composer require bnnvara/simple-bus-aws-bridge-bundle
        ```
      - Add injecting service bus into `CommentPostEndpoint` in `config/services.yaml` and `CommentPostEndpoint` constructor
      - Create topic in AWS SNS service
      - Create queue in AWS SQS service
      - Make queue subscription on topic
      - Create user in AWS IAM service
      - Copy AWS credentials in your .env.local file (see AWS steps below)
        * Copy **Access key ID** in .env.local `AWS_KEY`
        * Copy **Secret access key** in .env.local `AWS_SECRET`
        * Copy region set in AWS top right corner in .env.local `AWS_REGION`
        * Copy topic from SNS in .env.local `AWS_TOPIC_ARN`
        * Set **2010-03-31** in .env.local `AWS_SDK_VERSION`

  - Test sending messages.
    * Add test method `endpointCreatesAnEventOnTheEventBus` in `tests/Comment/Infrastructure/CommentPostEndpointTest.php`
    * Temporary copy your AWS environment from .env.local in .env.test file
    * Open AWS SQS service page. There in list of created queues find your `adapter-comment` queue
    * open contest menu by mouse right button clicking on item of list
    * Click on **View/Delete Messages**
    * Click **Start Polling for Messages** in opened page
    * Run `CommentPostEndpointTest`. Of everything correct you will see received message on AWS SQS page

    To ensure that both services are working correctly:

    * Open Adapter project
    * Change nginx exposed port in `docker-compose.yml` from 8000 on 8001
    * Comment block for DB service
    * Comment rows where set `depends_on: - db`
    * Execute `docker-compose up` in terminal with opened adapter project. Both projects should be working.
    * Open new terminal
    * From this terminal get access in terminal of php container by execution command
      ```bash
      docker exec -it adapter_php_1 bash
      ```
    * Execute command in containers terminal to subscribe on created topic in SNS service `adapter-comment`
      ```bash
      bin/console bnnvara:simplebus:consume adapter-comment
      ```
    * Check your `comment` database. There should appear record created after receiving message. You can repeat API test `CommentPostEndpointTest` to check records creating again.

    Creating events should be excluded for base test suit. So lets create mock for event bus to skip using remote third party service in test but add integration folder with this functionality for manual testing when we need it.

    * Add common service mock factory `tests/Comment/Infrastructure/Mocks/GenericMockFactory.php` for creating mocks from DI container to skip using real event bus service on it's automatic injecting in `CommentEndpointController` after receiving test requests.
    * Add test environment service configuration in `config/services_test.yaml` to overwrite default EventBus alias and add new alias on origin EventBus for manual integration testing. For service alias overwriting it should be set as public at first.
      ```yml
      simple_bus.event_bus:
          class: SimpleBus\SymfonyBridge\Bus\EventBus
          public: true

        simple_bus.event_bus.orig:
          alias: simple_bus.event_bus.mock.inner

        simple_bus.event_bus.mock:
          decorates: simple_bus.event_bus
          class: SimpleBus\SymfonyBridge\Bus\EventBus
          factory: ['App\Tests\Comment\Infrastructure\Mocks\GenericMockFactory', create]
          arguments:
            $className: SimpleBus\SymfonyBridge\Bus\EventBus
      ```
    * Run set in `tests/Comment/Infrastructure/CommentPostEndpointTest.php` tests
    * Create integration test `tests/Integration/EventBus/EventBusTest.php`
    * Run integration test

  - Add using UUID for possibility to update/delete existing comments. I'm use UUID's for generating record identifier before it saving in DB which will generate auto incremented id. Also it's safer then integer ID for displaying it to users because it can prevent grabbing your whole site by using default ID sequence. Uuid generating method we add info Infrastructure layer because generating is depended on environment (machine current time) and it used for storage functionality which belongs to infrastructure layer. So uuid generating belongs to infrastructure layer to. Method we will add in `CommentRepository` and give name for `nextIdentity`.
    * Install `polyfill-uuid` library for UUID generating  `symfony/polyfill-uuid`
    * Add test method `nextIdentity` in `tests/Comment/Infrastructure/CommentRepositoryTest.php`.
    * Add method `nextIdentity` into `CommentRepositoryInterface`
    * Add method `nextIdentity` into `CommentRepository`
    * Add injecting `CommentRepository` in `src/Comment/Infrastructure/CommentPostEndpoint.php` by adding it in `config/services.yaml` config
    * Add using new **commentId**, **createdAt** and **updatedAt** properties in all tests, value objects and Comment entity
    * Add new **commentId**, property in request/response json examples, Fixture `tests/Comment/Infrastructure/Fixtures`
    * Add **commentId**, **createdAt**, **updatedAt** properties in `src/Comment/Infrastructure/config/orm/Comment.orm.xml`
    * Add validation of input props in `src/Comment/Domain/ValueObject/Comment.php`
    * Update `src/Comment/Infrastructure/config/orm/Comment.orm.xml` in API and Adapter service
    * Add **commentId**, **createdAt**, **updatedAt** properties in Adapter service Comment entity
    * Rollback current migration which will delete already created tables `bin/console doctrine:migrations:migrate first`
    * Remove current migration
    * Generate new migration by executing bash command in adapter directory `bin/console doctrine:migrations:diff`
    * Execute migration in adapter service directory `bin/console doctrine:migrations:migrate`
    * Add using `final` for classes as one of best practices from [Matthias Noback](https://matthiasnoback.nl/2018/09/final-classes-by-default-why/)

### Setup AWS services

* Login into aws
* Check region which you use in right top corner of top menu. Use location closer to your users
* Open services lists (I setup displaying the button in the top of admin panel)

#### SNS - Simple Notification Service

* In search form enter `sns` and open it
* Click on **Create topic** button
* Enter **Name** and **Display name** of your topic. In our case we use `CommentUpsertedEvent`.
  Recommended to set suffix of environment which will use this topic (-dev, -prod etc)
* Click on **Create topic** button

#### SQS - Simple Queue Service

* In search form enter `sqs` and open it
* Click on **Get Started Now** button
* Type Queue name.
* By default is selected standard queue.
* Click on **Quick-Create Queue** button.
* In list of created queues make mouse right button click on newly created queue and click on `Subscribe Queue to SNS Topic` from displayed context menu
* In opened form choose region which you use when create queue if there is set another by default and choosetopic from dropdown list which will display after click on **Choose a Topic** input field.
* Click **Subscribe** button

#### IAM - Add User for getting credentials

* In search form enter `iam` and click in found item to open this service panel
* In opened page click on `Users` in left panel
* Click on **Add user** button
* Enter user name. You can use something like `ci-comments-app`. I use `ci-` prefix in names to identify that it is console interface user.
* Enable **Programmatic access** checkbox and click on button `Next: Permissions`
* In new opened page click on **Create Group**
* Type name for new group. To detect permissions enabled for this group you can set name like "SQS-SNS" because we will enable access on SQS and SNS services for this group.
* In input field bellow type SNS and choose **AmazonSNSFullAccess**. Than do the same for service SQS.
* Click on **Next: Tags** button
* In this form you can set tags which latter can be used in different proposes like tracking billing. For now I just skip it and click button **Next: Review**
* On new opened page click button **Create user**
* Now you can see your new users credentials.



---

## Issues

Issue with default symfony 5 packages dependencies. Some of them use `PHPUnit ^9.0` but `symfony/phpunit-bundle` use `PHPUnit  ^7.5`.
That's why default command `bin/phpunit` generates exception
```bash
Call to undefined method PHPUnit\TextUI\TestRunner::doRun()
```

Auto-registering services in `config/services.yaml` is required because in symfony 5 without using autowire and auto-registration controllers throw exception like this. In version 4 this exceptions was absent

```bash
...Controller" has no container set, did you forget to define it as a service subscriber?
```

So use this default services autowire config in `config/services.yaml` with set `autowire: true`

```yml
 App\:
        resource: '../src/*'
        exclude: '../src/{DependencyInjection,Entity,Migrations,Tests,Kernel.php}'
```
---
