<?php

namespace App\Tests\Comment\Infrastructure\Fixtures;

use App\Comment\Domain\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;

final class CommentFixture extends AbstractFixture implements ORMFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $comment = new Comment(
      '19264c50-1fdb-4648-aa90-15f9bacf1928',
      'abc78928-2419-4d0f-bce3-f75e74a727ca',
      '8ddaae1f-70ba-4de7-b16b-b0f300222258',
      'Hi Andrii from comment fixture!'
    );

    $manager->persist($comment);
    $manager->flush();
  }
}
