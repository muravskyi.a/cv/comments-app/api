<?php

namespace App\Tests\Infrastructure;

use App\Tests\Comment\Infrastructure\Fixtures\CommentFixture;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

final class CommentEndpointTest extends WebTestCase
{
  use FixturesTrait;

  /** @test */
  public function ACommentIsRetrievedFromTheEndpoint()
  {
    $client = $this->createClient();

    $this->loadFixtures([CommentFixture::class]);

    $client->request('GET', '/api/comment/comment/19264c50-1fdb-4648-aa90-15f9bacf1928');
    $response = $client->getResponse()->getContent();

    $this->assertJson($response);
    $responseArray = json_decode($response, true);
    $expectedArray = json_decode(file_get_contents(__DIR__ . '/Fixtures/response.json'), true);

    $dateTimeProps = ['createdAt', 'updatedAt'];
    $dateTimeFormat = '/\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}/';

    foreach($expectedArray as $rootProp => $entityProps) {
      foreach($entityProps as $prop => $value) {

        if (in_array($prop, $dateTimeProps)) {
          $this->assertMatchesRegularExpression($dateTimeFormat, $responseArray[$rootProp][$prop]);
        } else {
          $this->assertEquals($value, $responseArray[$rootProp][$prop]);
        }

      }
    }
  }
}
