<?php

namespace App\Tests\Comment\Infrastructure;

use Assert\Assertion;
use App\Comment\Domain\Repository\CommentRepositoryInterface;
use App\Tests\Comment\Infrastructure\Fixtures\CommentFixture;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

final class CommentRepositoryTest extends WebTestCase
{
  use FixturesTrait;

  /** @test */
  public function aCommentIsRetrievedFromTheDatastore(): void
  {
    $this->loadFixtures([CommentFixture::class]);

    $repository = $this->getRepository();

    $comment = $repository->getCommentByCommentId('19264c50-1fdb-4648-aa90-15f9bacf1928');

    $this->assertTrue(Assertion::uuid($comment->getCommentId()));
    $this->assertEquals('abc78928-2419-4d0f-bce3-f75e74a727ca', $comment->getUserId());
    $this->assertEquals('8ddaae1f-70ba-4de7-b16b-b0f300222258', $comment->getTopicId());
    $this->assertEquals('Hi Andrii from comment fixture!', $comment->getComment());
  }

  /** @test */
  public function aProperCommentIdIsCreatedOnNextUuidCall(): void
  {
    $repository = $this->getRepository();

    $commentId = $repository->nextUuid();

    $this->assertTrue(Assertion::uuid($commentId));
  }

  private function getRepository(): CommentRepositoryInterface
  {
    return $this->bootKernel()->getContainer()->get('Test.CommentRepository');
  }
}
