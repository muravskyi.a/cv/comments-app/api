<?php

namespace App\Comment\Infrastructure;

use App\Comment\Domain\ValueObject\CommentUpsertedEvent;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use SimpleBus\SymfonyBridge\Bus\EventBus;
use Symfony\Component\HttpFoundation\Request;

final class CommentPostEndpointTest extends WebTestCase
{
  /** @test */
  public function aCommentCanBePosted()
  {
    $client = $this->createClient();

    $client->request(
      'POST',
      '/api/comment/comment',
      [
        'commentId' => null,
        'topicId' => '8ddaae1f-70ba-4de7-b16b-b0f300222258',
        'userId' => 'abc78928-2419-4d0f-bce3-f75e74a727ca',
        'comment' => 'Hi from post test!'
      ]
    );

    $this->assertEquals('200', $client->getResponse()->getStatusCode());
    $this->assertEquals('success', $client->getResponse()->getContent());
  }

  /** @test */
  public function endpointReturnsFailedWhenNoPostDataGiven()
  {
    $client = $this->createClient();

    $client->request(
      'POST',
      '/api/comment/comment',
      []
    );

    $this->assertEquals('200', $client->getResponse()->getStatusCode());
    $this->assertEquals('failed', $client->getResponse()->getContent());
  }

  /** @test */
  public function endpointCreatesAnEventOnTheEventBus()
  {
    $container = $this->bootKernel()->getContainer();

    $commentRepository = $container->get('Test.CommentRepository');
    $commentValObjFactory = $container->get('Test.CommentValueObjectFactory');
    $commentUpsertedEventValObjFactory = $container->get('Test.CommentUpsertedEventValueObjectFactory');

    /** @var EventBus&\PHPUnit\Framework\MockObject\MockObject $eventBusMock */
    $eventBusMock = $container->get('simple_bus.event_bus');

    $eventBusMock->expects($this->once())
      ->method('handle')
      ->with($this->isInstanceOf(CommentUpsertedEvent::class));

    $endpoint = new CommentPostEndpoint(
      $commentRepository,
      $commentValObjFactory,
      $commentUpsertedEventValObjFactory,
      $eventBusMock
    );

    $requestJson = file_get_contents(__DIR__ . '/Fixtures/request.json');
    $requestData = json_decode($requestJson, true);

    $request = new Request([], $requestData);

    $response = $endpoint->sendCommentToEventBus($request);

    $this->assertEquals('success', $response->getContent());
  }
}
