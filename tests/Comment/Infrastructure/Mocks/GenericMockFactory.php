<?php

namespace App\Tests\Comment\Infrastructure\Mocks;

use PHPUnit\Framework\TestCase;

final class GenericMockFactory extends TestCase {
  public static function create(string $className)
  {
    return (new static)->createMock($className);
  }
}
