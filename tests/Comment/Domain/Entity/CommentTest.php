<?php

namespace App\Tests\Comment\Domain\Entity;

use App\Comment\Domain\Entity\Comment;
use PHPUnit\Framework\TestCase;

final class CommentTest extends TestCase
{
  private string $commentId = '19264c50-1fdb-4648-aa90-15f9bacf1928';
  private string $userId = 'abc78928-2419-4d0f-bce3-f75e74a727ca';
  private string $topicId = '8ddaae1f-70ba-4de7-b16b-b0f300222258';
  private string $comment = 'Test entity comment';

  /** @test */
  public function aCommentCanBeCreated()
  {
    $commentEntity = $this->buildEntity();

    $this->assertInstanceOf(Comment::class, $commentEntity);
    $this->assertEquals($this->commentId, $commentEntity->getCommentId());
    $this->assertEquals($this->userId, $commentEntity->getUserId());
    $this->assertEquals($this->topicId, $commentEntity->getTopicId());
    $this->assertEquals($this->comment, $commentEntity->getComment());
    $this->assertNull($commentEntity->getCreatedAt());
    $this->assertNull($commentEntity->getUpdatedAt());
  }

  /** @test */
  public function aCommentCanBeSerialized()
  {
    $comment = $this->buildEntity();

    $this->assertJsonStringEqualsJsonString(
      <<<END
      {
        "comment": {
          "commentId":"$this->commentId",
          "userId":"$this->userId",
          "topicId":"$this->topicId",
          "comment":"$this->comment",
          "createdAt":null,
          "updatedAt":null
        }
      }
      END,
      json_encode($comment)
    );
  }

  private function buildEntity(): Comment
  {
    return new Comment(
      $this->commentId,
      $this->userId,
      $this->topicId,
      $this->comment
    );
  }
}
