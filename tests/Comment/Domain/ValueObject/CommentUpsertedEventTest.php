<?php

namespace App\Tests\Comment\Domain\ValueObject;

use App\Comment\Domain\ValueObject\Comment;
use App\Comment\Domain\ValueObject\CommentUpsertedEvent;
use PHPUnit\Framework\TestCase;

final class CommentUpsertedEventTest extends TestCase
{
  /** @test */
  public function aCommentUpsertedEventCanBeCreated()
  {
    $comment = new Comment(
      '19264c50-1fdb-4648-aa90-15f9bacf1928',
      '8ddaae1f-70ba-4de7-b16b-b0f300222258',
      'abc78928-2419-4d0f-bce3-f75e74a727ca',
      'Test entity comment'
    );

    $event = new CommentUpsertedEvent($comment);

    $this->assertEquals($comment, $event->getData());
  }
}
