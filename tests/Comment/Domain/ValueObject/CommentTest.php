<?php

namespace App\Tests\Comment\Domain\ValueObject;

use App\Comment\Domain\ValueObject\Comment;
use PHPUnit\Framework\TestCase;

final class CommentTest extends TestCase
{
  private string $commentId = '19264c50-1fdb-4648-aa90-15f9bacf1928';
  private string $userId = 'abc78928-2419-4d0f-bce3-f75e74a727ca';
  private string $topicId = '8ddaae1f-70ba-4de7-b16b-b0f300222258';
  private string $comment = 'Test val obj comment';

  /** @test */
  public function aCommentCanBeCreated()
  {
    $commentValObj = new Comment(
      $this->commentId,
      $this->userId,
      $this->topicId,
      $this->comment
    );

    $this->assertInstanceOf(Comment::class, $commentValObj);
    $this->assertEquals($this->commentId, $commentValObj->getCommentId());
    $this->assertEquals($this->userId, $commentValObj->getUserId());
    $this->assertEquals($this->topicId, $commentValObj->getTopicId());
    $this->assertEquals($this->comment, $commentValObj->getComment());
  }
}
