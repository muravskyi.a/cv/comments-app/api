<?php

namespace App\Tests\Comment\Domain\ValueObjectFactory;

use App\Comment\Domain\ValueObject\Comment;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class CommentFactoryTest extends KernelTestCase
{
  /** @test */
  public function itCreatesCommentFromDataProps()
  {
    $commentId = '19264c50-1fdb-4648-aa90-15f9bacf1928';
    $userId = 'abc78928-2419-4d0f-bce3-f75e74a727ca';
    $topicId = '8ddaae1f-70ba-4de7-b16b-b0f300222258';
    $comment = 'Test val obj comment';

    $factory = $this->bootKernel()
      ->getContainer()
      ->get('Test.CommentValueObjectFactory');

    $commentEntity = $factory->createFromDataProps($commentId, $userId, $topicId, $comment);

    $this->assertInstanceOf(Comment::class, $commentEntity);
    $this->assertEquals($commentId, $commentEntity->getCommentId());
    $this->assertEquals($userId, $commentEntity->getUserId());
    $this->assertEquals($topicId, $commentEntity->getTopicId());
    $this->assertEquals($comment, $commentEntity->getComment());
  }
}
