<?php

namespace App\Test\Integration\EventBus;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class EventBusTest extends KernelTestCase
{
  /** @test */
  public function endpointCreatesAnEventOnTheEventBus()
  {
    $this->expectNotToPerformAssertions();

    $commentId = uuid_create(UUID_TYPE_RANDOM);
    $userId = uuid_create(UUID_TYPE_RANDOM);
    $topicId = uuid_create(UUID_TYPE_RANDOM);
    $comment = 'Hi from integration test of EventBus!';

    $container = $this->bootKernel()->getContainer();

    $eventBus = $container->get('simple_bus.event_bus.orig');
    $commentFactory = $container->get('Test.CommentValueObjectFactory');
    $commentUpsertedEventFactory = $container->get('Test.CommentUpsertedEventValueObjectFactory');

    $commentValObj = $commentFactory->createFromDataProps($commentId, $userId, $topicId, $comment);
    $commentUpsertedEvent = $commentUpsertedEventFactory->createFromCommentDataObject($commentValObj);

    $eventBus->handle($commentUpsertedEvent);
  }
}
