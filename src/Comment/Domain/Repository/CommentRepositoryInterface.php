<?php

namespace App\Comment\Domain\Repository;

use App\Comment\Domain\Entity\Comment;

interface CommentRepositoryInterface
{
  public function getCommentByCommentId(string $commentId): Comment;
  public function nextUuid(): string;
}
