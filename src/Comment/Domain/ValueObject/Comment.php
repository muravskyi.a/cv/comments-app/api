<?php

namespace App\Comment\Domain\ValueObject;

use Assert\Assertion;

final class Comment
{
  private string $commentId;
  private string $userId;
  private string $topicId;
  private string $comment;

  public function __construct(string $commentId, string $userId, string $topicId, string $comment)
  {
    Assertion::uuid($commentId);
    Assertion::uuid($userId);
    Assertion::uuid($topicId);
    Assertion::string($comment);

    $this->commentId = $commentId;
    $this->userId = $userId;
    $this->topicId = $topicId;
    $this->comment = $comment;
  }

  public function getCommentId(): string
  {
    return $this->commentId;
  }

  public function getUserId(): string
  {
    return $this->userId;
  }

  public function getTopicId(): string
  {
    return $this->topicId;
  }

  public function getComment(): string
  {
    return $this->comment;
  }
}
