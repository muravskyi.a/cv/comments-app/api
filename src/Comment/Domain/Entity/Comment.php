<?php

namespace App\Comment\Domain\Entity;

use DateTime;

final class Comment implements \JsonSerializable
{
  private int $id;
  private string $commentId;
  private string $userId;
  private string $topicId;
  private string $comment;
  private ?DateTime $createdAt = null;
  private ?DateTime $updatedAt = null;

  public function __construct(
    string $commentId,
    string $userId,
    string $topicId,
    string $comment
    )
  {
    $this->commentId = $commentId;
    $this->userId = $userId;
    $this->topicId = $topicId;
    $this->comment = $comment;
  }

  public function getId(): int
  {
    return $this->id;
  }

  public function getCommentId(): string
  {
    return $this->commentId;
  }

  public function getUserId(): string
  {
    return $this->userId;
  }

  public function getTopicId(): string
  {
    return $this->topicId;
  }

  public function getComment(): string
  {
    return $this->comment;
  }

  public function getCreatedAt(): ?DateTime
  {
    return $this->createdAt;
  }

  public function getUpdatedAt(): ?DateTime
  {
    return $this->updatedAt;
  }

  public function updateTimestamps(): void
  {
    $now = new \DateTime('now');

    $this->setUpdatedAt($now);

    if ($this->getCreatedAt() === null) {
        $this->setCreatedAt($now);
    }
  }

  public function jsonSerialize(): array
  {
    return [
      'comment' => [
        'commentId' => $this->getCommentId(),
        'userId' => $this->getUserId(),
        'topicId' => $this->getTopicId(),
        'comment' => $this->getComment(),
        'createdAt' => $this->createdAt ? $this->getCreatedAt()->format(DateTime::ATOM) : null,
        'updatedAt' => $this->updatedAt ? $this->getUpdatedAt()->format(DateTime::ATOM) : null,
      ]
    ];
  }

  private function setCreatedAt(DateTime $createdAt): void
  {
    $this->createdAt = $createdAt;
  }

  private function setUpdatedAt(DateTime $updatedAt): void
  {
    $this->updatedAt = $updatedAt;
  }
}
