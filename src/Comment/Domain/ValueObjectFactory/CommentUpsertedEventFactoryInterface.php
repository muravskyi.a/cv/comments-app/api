<?php

namespace App\Comment\Domain\ValueObjectFactory;

use App\Comment\Domain\ValueObject\Comment;
use App\Comment\Domain\ValueObject\CommentUpsertedEvent;

interface CommentUpsertedEventFactoryInterface
{
  public function createFromCommentDataObject(Comment $comment): CommentUpsertedEvent;
}
