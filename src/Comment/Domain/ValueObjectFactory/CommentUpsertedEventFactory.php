<?php

namespace App\Comment\Domain\ValueObjectFactory;

use App\Comment\Domain\ValueObject\Comment;
use App\Comment\Domain\ValueObject\CommentUpsertedEvent;

final class CommentUpsertedEventFactory implements CommentUpsertedEventFactoryInterface
{
  public function createFromCommentDataObject(Comment $comment): CommentUpsertedEvent
  {
    return new CommentUpsertedEvent($comment);
  }
}
