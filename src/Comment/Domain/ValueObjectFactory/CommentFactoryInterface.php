<?php

namespace App\Comment\Domain\ValueObjectFactory;

use App\Comment\Domain\ValueObject\Comment;

interface CommentFactoryInterface
{
  public function createFromDataProps(string $uuid, string $userId, string $topicId, string $comment): Comment;
}
