<?php

namespace App\Comment\Domain\ValueObjectFactory;

use App\Comment\Domain\ValueObject\Comment;

final class CommentFactory implements CommentFactoryInterface
{
  public function createFromDataProps(string $commentId, string $userId, string $topicId, string $comment): Comment
  {
    return new Comment($commentId, $userId, $topicId, $comment);
  }
}
