<?php

namespace App\Comment\Infrastructure;

use App\Comment\Domain\Repository\CommentRepositoryInterface;
use App\Comment\Domain\ValueObjectFactory\commentFactoryInterface as CommentValObjFactoryInterface;
use App\Comment\Domain\ValueObjectFactory\CommentUpsertedEventFactoryInterface as CommentUpsertedEventValObjFactoryInterface;
use SimpleBus\SymfonyBridge\Bus\EventBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CommentPostEndpoint extends AbstractController
{
  private $commentRepository;
  private $commentValObjFactory;
  private $commentUpsertedEventValObjFactory;
  private $eventBus;

  public function __construct(
    CommentRepositoryInterface $commentRepository,
    CommentValObjFactoryInterface $commentValObjFactory,
    CommentUpsertedEventValObjFactoryInterface $commentUpsertedEventValObjFactory,
    EventBus $eventBus
  ) {
    $this->commentRepository = $commentRepository;
    $this->commentValObjFactory = $commentValObjFactory;
    $this->commentUpsertedEventValObjFactory = $commentUpsertedEventValObjFactory;
    $this->eventBus = $eventBus;
  }

  public function sendCommentToEventBus(Request $request): Response
  {
    if (!$this->requestIsValid($request)) {
      return new Response('failed');
    }

    $comment = $this->commentValObjFactory->createFromDataProps(
      $request->request->get('commentId') ?: $this->commentRepository->nextUuid(),
      $request->request->get('userId'),
      $request->request->get('topicId'),
      $request->request->get('comment')
    );

    $commentUpsertedEvent = $this->commentUpsertedEventValObjFactory->createFromCommentDataObject($comment);

    $this->eventBus->handle($commentUpsertedEvent);

    return new Response('success');
  }

  private function requestIsValid(Request $request): bool
  {
    return !empty($request->request->get('userId'))
      && !empty($request->request->get('topicId'))
      && !empty($request->request->get('comment'));
  }
}
