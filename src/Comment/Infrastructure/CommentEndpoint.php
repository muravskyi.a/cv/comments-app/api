<?php

namespace App\Comment\Infrastructure;

use App\Comment\Domain\Repository\CommentRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

final class CommentEndpoint extends AbstractController
{
  private $commentRepository;

  public function __construct(CommentRepositoryInterface $commentRepository)
  {
    $this->commentRepository = $commentRepository;
  }

  public function getCommentAsJson(string $commentId) : JsonResponse
  {
    $comment = $this->commentRepository->getCommentByCommentId($commentId);

    return new JsonResponse($comment);
  }
}
