<?php

namespace App\Comment\Infrastructure;

use App\Comment\Domain\Entity\Comment;
use App\Comment\Domain\Repository\CommentRepositoryInterface;
use Doctrine\ORM\EntityRepository;

final class CommentRepository extends EntityRepository implements CommentRepositoryInterface
{
  public function getCommentByCommentId(string $commentId) : Comment
  {
    return $this->findOneBy(['commentId' => $commentId]);
  }

  public function nextUuid(): string
  {
    return uuid_create(UUID_TYPE_RANDOM);
  }
}
