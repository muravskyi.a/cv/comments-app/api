<?php

namespace App\IAmAlive;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class IAmAliveController extends AbstractController
{
  public function index(Request $request): Response
  {
    return new Response('I-am-alive');
  }
}
